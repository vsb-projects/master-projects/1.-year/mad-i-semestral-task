package main;

import algorithms.K_Means;
import algorithms.Probability;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.parser.Path;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;


import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import utils.Data;
import utils.DataRow;
import utils.DataType;
import utils.DataValue;

import javax.imageio.ImageIO;
import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Controller {

    private Stage primaryStage;

    /**
     * UI
     */
    public BorderPane mainPane;
    public AnchorPane anchorPane;
    public Button chooseButton;

    public Stage fileChooseStage;
    public Label fileLabel;
    public Button kMeansButton;

    public TableView<ObservableList<String>> initDataTable = new TableView<>();
    public ComboBox probabilityComboBox;
    public Button probabilityButton;


    /**
     * FUNC
     */
    public Data dataStore = null;
    private Map<DataRow, List<DataRow>> clusters = new HashMap<>();

    public File chosenFile = null;
    public TextField clusterNumberTextField;
    public Button SSEButton;

    private static DecimalFormat df = new DecimalFormat("0.00");


    public void onFileChoose(ActionEvent actionEvent) throws IOException {

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select dataset");

        fileChooseStage = new Stage();

        chosenFile = fileChooser.showOpenDialog(fileChooseStage);

        if(chosenFile.exists()) {

            fileLabel.setText(chosenFile.getName());

            this.dataStore = new Data();

            dataStore.parseCSVData(chosenFile.getAbsolutePath());

//        dataStore.displayData();

            initDataTable.getItems().clear();
            initDataTable.getColumns().clear();

            buildInitDataTable();

            String numberAttributes[] = this.dataStore.getNumberAttributes().toArray(new String[0]);
//        String attributes[] = this.dataStore.getAttributes().toArray(new String[0]);

            // Create a combo box
            this.probabilityComboBox.setItems(FXCollections.observableArrayList(numberAttributes));
//        this.probabilityComboBox.setItems(FXCollections.observableArrayList(attributes));

            this.primaryStage.setWidth(initDataTable.getWidth());
        }

    }

    public void onClose(ActionEvent actionEvent) {
        Platform.exit();
        System.exit(0);
    }

    public void onKMeans(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.ERROR);

        if(chosenFile == null || !chosenFile.exists()) {
            alert.setContentText("Please choose file first first!");
            alert.show();
        } else {

            if (this.clusterNumberTextField.getText().isEmpty()) {
                alert.setContentText("Please set number of clusters first!");
                alert.show();
            } else {

                Integer K = 0;

                try {
                    K = Integer.parseInt(this.clusterNumberTextField.getText());
                } catch (Exception e) {
                    alert.setContentText("Set valid number!");
                    alert.show();
                }

                K_Means k_means = new K_Means(K, dataStore.getDataRows());
                k_means.countKMeans(false);


                this.clusters = k_means.getClusters();


                /**
                 * Display K Means
                 */
                Stage stage = new Stage();
                stage.setTitle("K Means clusters");


                VBox clustersVBox = buildClustersView(k_means, true);


                ScrollPane scroller = new ScrollPane();
                scroller.setContent(clustersVBox);
                scroller.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);


                VBox mainBox = new VBox();
                mainBox.getChildren().addAll(this.buildShowChartOptions(), scroller);

                Scene scene = new Scene(mainBox);

//        ((Group) scene.getRoot()).getChildren().addAll(scroller);


                stage.setScene(scene);
                stage.show();
            }
        }
    }

    private VBox buildClustersView(K_Means k_means, boolean withAllData) {
        final VBox vbox = new VBox();
        vbox.setSpacing(5);
//        vbox.setPrefHeight(Region.USE_COMPUTED_SIZE);
//        vbox.setPrefWidth(Region.USE_COMPUTED_SIZE);
        vbox.setPadding(new Insets(10, 0, 0, 10));

        int index = 1;
        // Table
        for (Map.Entry<DataRow, List<DataRow>> map : k_means.getClusters().entrySet()) {
            TableView<ObservableList<String>> clusterTable = new TableView<>();
            TableView<ObservableList<String>> clusterTableItems = new TableView<>();
            clusterTable.setEditable(true);
            clusterTable.setPrefHeight(100.0);

            buildTableHeader(clusterTable);
            buildTableHeader(clusterTableItems);


            clusterTable.getItems().add(
                    FXCollections.observableArrayList(
                            map.getKey().getAllDataAsString()
                    )
            );

            // ADD CLUSTER MEAN TO CLUSTER TABLE
            List<String> stringList = new ArrayList<>();
            int addIndex = 0;

            for(int i = 0; i < map.getKey().getDataAllValues().size();i++){
                if(map.getKey().getDataAllValues().get(i).getDataType().equals(DataType.STRING)){
                    stringList.add((String)map.getKey().getDataAllValues().get(i).getData());
                } else {
                    stringList.add(df.format(k_means.getClustersMean().get(map.getKey()).get(addIndex)).toString());
                    addIndex++;
                }
            }

            clusterTable.getItems().add(
                    FXCollections.observableArrayList(stringList)
            );


            // CLUSTER ITEMS
            for (DataRow dataRow : map.getValue()) {
                clusterTableItems.getItems().add(
                        FXCollections.observableArrayList(
                                dataRow.getAllDataAsString()
                        )
                );
            }


            Label clusterSizeLabel = new Label();
            clusterSizeLabel.setText("Size: " + map.getValue().size());
            clusterSizeLabel.setStyle(
                    "    -fx-font-size: 16pt;\n" +
                            "    -fx-font-family: \"Segoe UI Light\";\n" +
                            "    -fx-opacity: 1;"
            );


            Label label = new Label("Cluster " + index);
            label.setFont(new Font("Arial", 25));
            label.setStyle("-fx-font-weight: bold;");
//                label.setPadding(new Insets(20, 0, 5, 0));

            HBox clusterHBox = new HBox();
            clusterHBox.setSpacing(20);
            clusterHBox.setAlignment(Pos.CENTER_LEFT);
            clusterHBox.getChildren().addAll(label, clusterSizeLabel);

            vbox.getChildren().add(clusterHBox);
            vbox.getChildren().add(clusterTable);

            if(withAllData) {
                vbox.getChildren().add(clusterTableItems);
            }
            index++;
        }

        return vbox;
    }

    public void buildInitDataTable(){
        initDataTable.setEditable(true);
        initDataTable.setPrefWidth(Region.USE_COMPUTED_SIZE);

        buildTableHeader(initDataTable);

        for(DataRow dataRow : this.dataStore.getDataRows()){
            initDataTable.getItems().add(
                    FXCollections.observableArrayList(dataRow.getAllDataAsString())
            );
        }
    }

    public void buildTableHeader(TableView table){
        int i = 0;
        for(String attr : this.dataStore.getAttributes()){
            final int finalIdx = i;
            TableColumn<ObservableList<String>, String> column = new TableColumn<>(attr);
            column.setCellValueFactory(param ->
                    new ReadOnlyObjectWrapper<>(param.getValue().get(finalIdx))
            );
            table.getColumns().add(column);
            i++;
        }
    }

    public void onSSE(ActionEvent actionEvent) {
        if(chosenFile == null || !chosenFile.exists()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Please choose file first first!");
            alert.show();
        } else {

            if (this.clusterNumberTextField.getText().isEmpty()) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Please set number of clusters first!");
                alert.show();
            } else {

                Integer K = 0;

                try {
                    K = Integer.parseInt(this.clusterNumberTextField.getText());
                } catch (Exception e) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setContentText("Set valid number!");
                    alert.show();
                }

                K_Means k_means = new K_Means(K, dataStore.getDataRows());
                k_means.countKMeans(false);

                Double SSE = k_means.countSSE();

                System.out.println("SSE: " + SSE);


                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText("SSE");
                alert.setContentText("SSE: " + SSE);
                alert.show();
            }
        }
    }

    public HBox buildShowChartOptions(){
        Label description_label = new Label("Choose 2 attributes");


        String attributes[] = this.dataStore.getNumberAttributes().toArray(new String[0]);

        // Create a combo box
        ComboBox attrComboBoxFirst = new ComboBox(FXCollections.observableArrayList(attributes));
        ComboBox attrComboBoxSecond = new ComboBox(FXCollections.observableArrayList(attributes));

        Button button = new Button("Generate graph");
        button.setOnAction(event -> {
            this.onGenerateKMeansGraph(String.valueOf(attrComboBoxFirst.getValue()), String.valueOf(attrComboBoxSecond.getValue()), true);
        });

        HBox comboHBox = new HBox();
        comboHBox.setSpacing(20.0);
        comboHBox.getChildren().addAll(attrComboBoxFirst,attrComboBoxSecond,button);


        return comboHBox;
    }

    public ScatterChart onGenerateKMeansGraph(String firstAttr, String secondAttr, boolean doDisplay) {
//        List<DataValue> firstList = dataStore.getDataRows().stream().map(m -> m.getDataAllValues().stream().filter(f -> f.getAttribute().equals(firstAttr)).findAny().get()).collect(Collectors.toList());
//        List<DataValue> secondList = dataStore.getDataRows().stream().map(m -> m.getDataAllValues().stream().filter(f -> f.getAttribute().equals(secondAttr)).findAny().get()).collect(Collectors.toList());

        if(!this.dataStore.getDataRows().isEmpty()) {


        Stage graphStage = new Stage();
        graphStage.setTitle("K means graph");
        final NumberAxis xAxis = new NumberAxis(0, 100, 10);
        final NumberAxis yAxis = new NumberAxis(0, 100, 10);

        xAxis.setAnimated(true);
        yAxis.setAnimated(true);

        xAxis.setLabel(firstAttr);
        yAxis.setLabel(secondAttr);

        final ScatterChart<Number,Number> sc = new
                ScatterChart<Number,Number>(xAxis,yAxis);
        sc.setTitle("Clusters data overview");


            List<DataValue> firstList;
            List<DataValue> secondList;

            Integer graphAxisSize = 0;

            int count = 0;

            for (Map.Entry<DataRow, List<DataRow>> cluster : this.clusters.entrySet()) {
                firstList = cluster.getValue().stream().map(m -> m.getDataAllValues().stream().filter(f -> f.getAttribute().equals(firstAttr)).findAny().get()).collect(Collectors.toList());
                secondList = cluster.getValue().stream().map(m -> m.getDataAllValues().stream().filter(f -> f.getAttribute().equals(secondAttr)).findAny().get()).collect(Collectors.toList());

                XYChart.Series series = new XYChart.Series();
                series.setName("Cluster " + (count+1));

                for (int i = 0; i < firstList.size(); i++) {
                    series.getData().add(new XYChart.Data(firstList.get(i).getData(), secondList.get(i).getData()));
                }

                sc.getData().add(series);

                Integer maxFirst = (int) Math.round((double) firstList.stream().max(Comparator.comparing(u -> Double.valueOf((double) u.getData()))).get().getData());
                Integer maxSecond = (int) Math.round((double) secondList.stream().max((u,v) -> Double.valueOf((double) u.getData()).compareTo(Double.valueOf((double) v.getData()))).get().getData());

                if(graphAxisSize < maxFirst){
                    graphAxisSize = maxFirst;
                }
                if(graphAxisSize < maxSecond){
                    graphAxisSize = maxSecond;
                }

                count++;
            }

            graphAxisSize += 20;

            yAxis.setUpperBound(graphAxisSize);
            xAxis.setUpperBound(graphAxisSize);

            Scene scene = new Scene(sc, 1500, 1500);

            if(doDisplay) {
                graphStage.setScene(scene);
                graphStage.show();
            }
            return sc;
        }

        return null;
    }

    public void onProbabilityComboBox(ActionEvent actionEvent) {
    }

    public void onCalculateProbability(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.ERROR);

        if(chosenFile == null || !chosenFile.exists()) {
            alert.setContentText("Please choose file first first!");
            alert.show();
        } else if (this.probabilityComboBox.getValue() == null){
            alert.setContentText("Please select probability attribute");
            alert.show();
        } else {

            String chosenAttr = (String) this.probabilityComboBox.getValue();

//        int column = this.dataStore.getAttributes().indexOf(chosenAttr);
            int column = this.dataStore.getNumberAttributes().indexOf(chosenAttr);

            Probability probability = new Probability(this.dataStore.getNumberAttributes(), this.dataStore.getDataRows());

            probability.computeProbability(column, true);
        }
    }


    public void setPrimaryStage(Stage primaryStage){
        this.primaryStage = primaryStage;
    }

    public void onExport(ActionEvent actionEvent) throws DocumentException, IOException, URISyntaxException {
        Alert alert = new Alert(Alert.AlertType.ERROR);

        if(chosenFile == null || !chosenFile.exists()) {
            alert.setContentText("Please choose file first first!");
            alert.show();
        } else if (this.clusterNumberTextField.getText().isEmpty()) {
            alert.setContentText("Please set number of clusters first!");
            alert.show();
        } else if (this.probabilityComboBox.getValue() == null){
            alert.setContentText("Please select probability attribute");
            alert.show();
        }


        /**
         * K - means
         */
        Integer K = 0;

        try {
            K = Integer.parseInt(this.clusterNumberTextField.getText());
        } catch (Exception e) {
            alert.setContentText("Set valid number!");
            alert.show();
        }

        K_Means k_means = new K_Means(K, dataStore.getDataRows());
        k_means.countKMeans(false);

        Double SSE = k_means.countSSE();

        this.clusters = k_means.getClusters();

        // render Clusters table
        VBox clustersVBox = this.buildClustersView(k_means,false);
        Scene scene = new Scene(clustersVBox);

        ByteArrayOutputStream ClustersByteOutput = new ByteArrayOutputStream();
        WritableImage clustersImg = clustersVBox.snapshot(new SnapshotParameters(), null);
        ImageIO.write( SwingFXUtils.fromFXImage( clustersImg, null ), "png", ClustersByteOutput );


        String firstAttr = this.dataStore.getNumberAttributes().get(new Random().nextInt(dataStore.getNumberAttributes().size()));
        String secondAttr = this.dataStore.getNumberAttributes().get(new Random().nextInt(dataStore.getNumberAttributes().size()));
        while(firstAttr.equals(secondAttr)){
            secondAttr = this.dataStore.getNumberAttributes().get(new Random().nextInt(dataStore.getNumberAttributes().size()));
        }

        ByteArrayOutputStream clustersGraph = new ByteArrayOutputStream();
        WritableImage clustersGraphImg = this.onGenerateKMeansGraph(firstAttr,secondAttr,false).snapshot(new SnapshotParameters(), null);
        ImageIO.write( SwingFXUtils.fromFXImage( clustersGraphImg, null ), "png", clustersGraph );


        /**
         * Probability
         */
        String chosenAttr = (String) this.probabilityComboBox.getValue();

        int column = this.dataStore.getNumberAttributes().indexOf(chosenAttr);

        Probability probability = new Probability(this.dataStore.getNumberAttributes(), this.dataStore.getDataRows());

        probability.computeProbability(column, false);


        // render probability graph
        WritableImage image = probability.getLineChart().snapshot(new SnapshotParameters(), null);
        File file = new File("probabilityChart.png");

        ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);





        // Create document
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream("MAD_I_Ultimate_analyzer_export.pdf"));
        document.open();

        // headline
        com.itextpdf.text.Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 20, BaseColor.BLACK);
        Paragraph mainHeadline = new Paragraph("MAD I Analysis export\n", font);
        mainHeadline.setAlignment(Element.ALIGN_CENTER);
        document.add(mainHeadline);
        document.add(Chunk.NEWLINE);


        // clusters
        font = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 16, BaseColor.BLACK);
        Paragraph clustersHeadline = new Paragraph("K means", font);
        document.add(clustersHeadline);

        Image clustersImage = Image.getInstance(ClustersByteOutput.toByteArray());
        clustersImage.scalePercent(50);
        document.add(clustersImage);
        document.add(Chunk.NEWLINE);

        font = FontFactory.getFont(FontFactory.HELVETICA, 12, BaseColor.BLACK);
        Paragraph SSEpar = new Paragraph("SSE " + SSE.toString(), font);
        document.add(SSEpar);
        document.add(Chunk.NEWLINE);

        Image clustersGraphImage = Image.getInstance(clustersGraph.toByteArray());
        clustersGraphImage.scalePercent(30);
        clustersGraphImage.setAlignment(Element.ALIGN_CENTER);
        document.add(clustersGraphImage);
        document.add(Chunk.NEWLINE);


        // Probability
        font = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 16, BaseColor.BLACK);
        Paragraph probabilityHeadline = new Paragraph("Probability --> " + chosenAttr, font);
        document.add(probabilityHeadline);


        font = FontFactory.getFont(FontFactory.HELVETICA, 12, BaseColor.BLACK);
        Paragraph probabilityData = new Paragraph(probability.getStringBuilder().toString(), font);
        document.add(probabilityData);
        document.add(Chunk.NEWLINE);

        Image probabilityGraphImg = Image.getInstance(file.getAbsolutePath());
        probabilityGraphImg.scalePercent(50);
        probabilityGraphImg.setAlignment(Element.ALIGN_CENTER);
        document.add(probabilityGraphImg);

        document.close();
    }

}
