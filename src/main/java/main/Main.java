package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader mainViewSceneLoader = new FXMLLoader(getClass().getClassLoader().getResource("sample.fxml"));
        Parent mainView = mainViewSceneLoader.load();
        primaryStage.setTitle("MAD I Ultimate analyzer");
        primaryStage.setScene(new Scene(mainView));
        primaryStage.show();

        Controller controller = mainViewSceneLoader.getController();
        controller.setPrimaryStage(primaryStage);
    }


    public static void main(String[] args) {
        launch(args);
    }
}
