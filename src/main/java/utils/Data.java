package utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Data {

    private List<String> attributes;

    private List<String> numberAttributes;

    private List<String> stringAttributes;

    private List<DataRow> dataRows;


    public void parseCSVData(String file) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

            String line;
            boolean firstLine = true;
            boolean secondLine = false;

            DataRow dataRow;

            Integer rowCount = 0;

            while((line = bufferedReader.readLine()) != null){
//                String[] lines = line.replace(',','.').split(";");
                String[] lines = line.split(",(?=(?:[^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)");

                dataRow = new DataRow(rowCount++);

                for(int i = 0; i < lines.length; i++) {
                    String l = lines[i];

                    if(firstLine){
                        this.attributes.add(l);
                    } else {
                        if(l == null || l.isEmpty()){
                            DataValue<Double> dataValue = new DataValue<>(DataType.NONE,attributes.get(i),null);
                            dataRow.addNullValue(dataValue);
                            dataRow.addNumberValue(dataValue);
                            dataRow.addValue(dataValue);
                        }else if(isNumeric(l)) {
                            DataValue<Double> dataValue = new DataValue<Double>(DataType.NUMBER,attributes.get(i),Double.valueOf(l));
                            dataRow.addNumberValue(dataValue);
                            dataRow.addValue(dataValue);
                        } else {
                            DataValue<String> dataValue = new DataValue<String>(DataType.STRING,attributes.get(i),String.valueOf(l));
                            dataRow.addStringValue(dataValue);
                            dataRow.addValue(dataValue);
                        }

                        if(secondLine) {
                            if (isNumeric(l)) {
                                this.numberAttributes.add(this.attributes.get(i));
                            } else {
                                this.stringAttributes.add(this.attributes.get(i));
                            }
                        }
                    }
                }

                if(secondLine){
                    secondLine = false;
                }

                if(firstLine) {
                    firstLine = false;
                    secondLine = true;
                } else {
                    this.dataRows.add(dataRow);
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

//        System.out.println(data.toString());
    }


    private static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }


    public void displayData(){
        StringBuilder stringBuilderAttr = new StringBuilder();
        StringBuilder stringBuilderRows = new StringBuilder();

        for(String attr : this.attributes){
            stringBuilderAttr.append(attr + "\t");
        }
        stringBuilderAttr.append("\n\n");

        for(DataRow row : this.dataRows){
            for(DataValue dataValue : row.getDataAllValues()){
                if(dataValue.getData() == null){
                    stringBuilderAttr.append(" \t");
                } else {
                    stringBuilderRows.append(dataValue.getData().toString() + "\t");
                }
            }
            stringBuilderRows.append("\n");
        }

        System.out.println(stringBuilderAttr.toString());
        System.out.println(stringBuilderRows.toString());
    }




    public Data() {
        this.attributes = new ArrayList<String>();
        this.numberAttributes = new ArrayList<String>();
        this.stringAttributes = new ArrayList<String>();
        this.dataRows = new ArrayList<DataRow>();
    }

    public Data(List<String> attributes, List<String> numberAttributes, List<String> stringAttributes, List<DataRow> dataRows) {
        this.attributes = attributes;
        this.numberAttributes = numberAttributes;
        this.stringAttributes = stringAttributes;
        this.dataRows = dataRows;
    }

    public List<String> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<String> attributes) {
        this.attributes = attributes;
    }

    public List<DataRow> getDataRows() {
        return dataRows;
    }

    public void setDataRows(List<DataRow> dataRows) {
        this.dataRows = dataRows;
    }

    public List<String> getNumberAttributes() {
        return numberAttributes;
    }

    public void setNumberAttributes(List<String> numberAttributes) {
        this.numberAttributes = numberAttributes;
    }

    public List<String> getStringAttributes() {
        return stringAttributes;
    }

    public void setStringAttributes(List<String> stringAttributes) {
        this.stringAttributes = stringAttributes;
    }
}
