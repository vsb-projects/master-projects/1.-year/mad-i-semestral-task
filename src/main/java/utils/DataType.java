package utils;

public enum DataType {
    NONE, STRING, NUMBER;
}
