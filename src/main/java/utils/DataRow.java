package utils;

import java.util.ArrayList;
import java.util.List;

public class DataRow {

    private Integer id;

    private List<DataValue> dataAllValues;

    private List<DataValue> dataStringValues;

    private List<DataValue> dataNumberValues;

    private List<DataValue> dataNullValues;



    public DataRow() {
        this.id = 0;
        this.dataStringValues = new ArrayList<DataValue>();
        this.dataNumberValues = new ArrayList<DataValue>();
        this.dataNullValues = new ArrayList<DataValue>();
        this.dataAllValues = new ArrayList<DataValue>();
    }

    public DataRow(Integer id) {
        this.id = id;
        this.dataStringValues = new ArrayList<DataValue>();
        this.dataNumberValues = new ArrayList<DataValue>();
        this.dataNullValues = new ArrayList<DataValue>();
        this.dataAllValues = new ArrayList<DataValue>();
    }

    public DataRow(Integer id, List<DataValue> dataAllValues, List<DataValue> dataStringValues, List<DataValue> dataNumberValues, List<DataValue> dataNullValues) {
        this.id = id;
        this.dataAllValues = dataAllValues;
        this.dataStringValues = dataStringValues;
        this.dataNumberValues = dataNumberValues;
        this.dataNullValues = dataNullValues;
    }

    public void addValue(DataValue dataValue){
        this.dataAllValues.add(dataValue);
    }

    public void addStringValue(DataValue dataStringValue){
        this.dataStringValues.add(dataStringValue);
    }

    public void addNumberValue(DataValue dataNumberValue) {
        this.dataNumberValues.add(dataNumberValue);
    }

    public void addNullValue(DataValue dataNullValue) {
        this.dataNullValues.add(dataNullValue);
    }


    public List<Double> getDoubleListFromData(){
        List<Double> doubleList = new ArrayList<>();
        for(DataValue dataValue : this.dataNumberValues){
            if(dataValue.getData() == null){
                doubleList.add(0.0);
            } else {
                doubleList.add((double) dataValue.getData());
            }
        }
        return doubleList;
    }


    public List<String> getAllDataAsString() {
      List<String> stringList = new ArrayList<>();
        for(DataValue dataValue : this.dataAllValues){
            if(dataValue.getData() == null){
                stringList.add("");
            } else {
                stringList.add(String.valueOf(dataValue.getData()));
            }
        }
        return stringList;
    };


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<DataValue> getDataStringValues() {
        return dataStringValues;
    }

    public void setDataStringValues(List<DataValue> dataStringValues) {
        this.dataStringValues = dataStringValues;
    }

    public List<DataValue> getDataNumberValues() {
        return dataNumberValues;
    }

    public void setDataNumberValues(List<DataValue> dataNumberValues) {
        this.dataNumberValues = dataNumberValues;
    }

    public List<DataValue> getDataNullValues() {
        return dataNullValues;
    }

    public void setDataNullValues(List<DataValue> dataNullValues) {
        this.dataNullValues = dataNullValues;
    }

    public List<DataValue> getDataAllValues() {
        return dataAllValues;
    }

    public void setDataAllValues(List<DataValue> dataAllValues) {
        this.dataAllValues = dataAllValues;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DataRow dataRow = (DataRow) o;

        if (id != null ? !id.equals(dataRow.id) : dataRow.id != null) return false;
        if (dataAllValues != null ? !dataAllValues.equals(dataRow.dataAllValues) : dataRow.dataAllValues != null)
            return false;
        if (dataStringValues != null ? !dataStringValues.equals(dataRow.dataStringValues) : dataRow.dataStringValues != null)
            return false;
        if (dataNumberValues != null ? !dataNumberValues.equals(dataRow.dataNumberValues) : dataRow.dataNumberValues != null)
            return false;
        return dataNullValues != null ? dataNullValues.equals(dataRow.dataNullValues) : dataRow.dataNullValues == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (dataAllValues != null ? dataAllValues.hashCode() : 0);
        result = 31 * result + (dataStringValues != null ? dataStringValues.hashCode() : 0);
        result = 31 * result + (dataNumberValues != null ? dataNumberValues.hashCode() : 0);
        result = 31 * result + (dataNullValues != null ? dataNullValues.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return  "id: " + id + " --> " + dataAllValues.toString();
    }
}
