package utils;

public class DataValue<T> {

    private DataType dataType;

    private String attribute;

    private T data;

    public DataValue() {
    }

    public DataValue(DataType dataType, String attribute, T data) {
        this.dataType = dataType;
        this.attribute = attribute;
        this.data = data;
    }

    public DataType getDataType() {
        return dataType;
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DataValue<?> dataValue = (DataValue<?>) o;

        if (dataType != dataValue.dataType) return false;
        if (attribute != null ? !attribute.equals(dataValue.attribute) : dataValue.attribute != null) return false;
        return data != null ? data.equals(dataValue.data) : dataValue.data == null;
    }

    @Override
    public int hashCode() {
        int result = dataType != null ? dataType.hashCode() : 0;
        result = 31 * result + (attribute != null ? attribute.hashCode() : 0);
        result = 31 * result + (data != null ? data.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return " " + data;
    }
}
