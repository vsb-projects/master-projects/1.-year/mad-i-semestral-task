package algorithms;

import java.util.List;

public class Euclidian_Cosine_Mean {


    public static Double countEuclideanDistanceOfTwoElements(List<Double> firstElem, List<Double> secondElem){
        Double result = 0.0;
        Double d = 0.0;

        for(int i = 0; i < firstElem.size(); i++){
            d = Math.pow(firstElem.get(i) - secondElem.get(i),2);
            result += d;
        }

        result = Math.sqrt(result);

        return result;
    }

    public void countCosineSimilarity(List<Double> firstElem, List<Double> secondElem) {

        Double result = 0.0;
        Double up = 0.0;
        Double down = 0.0;

        Double a = 0.0;
        Double b = 0.0;
        Double d;

        for (int i = 0; i < firstElem.size(); i++) {
            d = firstElem.get(i) * secondElem.get(i);
            up += d;
        }

        for (Double dou : firstElem) {
            a += Math.pow(dou, 2);
        }
        for (Double dou : secondElem) {
            b += Math.pow(dou, 2);
        }

        down = Math.sqrt(a) * Math.sqrt(b);

        result = up / down;

        System.out.println("Cosine similarity: " + result);
    }

    public Double countMeanIndex(int index, List<List<Double>> data){
        Double count = 0.0;

        for(List<Double> d : data){
            count += d.get(index);
        }

        return count/data.size();
    }

    public Double countTotalVariance(int row, List<List<Double>> data){

        Double sepalLengthMean = countMeanIndex(0,data);
        Double sepalWidthMean = countMeanIndex(1, data);
        Double petalLengthMean = countMeanIndex(2, data);
        Double petalWidthMean = countMeanIndex(3, data);

        Double count = 0.0;

        for(List<Double> rr : data){

            double sl = rr.get(0) - sepalLengthMean;
            double sw = rr.get(1) - sepalWidthMean;
            double pl = rr.get(2) - petalLengthMean;
            double pw = rr.get(3) - petalWidthMean;

            double res = Math.sqrt(Math.pow(sl,2) + Math.pow(sw,2) + Math.pow(pl,2) + Math.pow(pw,2));

            count += Math.pow(res,2);
        }


        System.out.println();
        System.out.println("Total Variance: " + count / data.size());
        System.out.println();


        return count / data.size();
    }
}
