package algorithms;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;
import utils.DataRow;
import utils.DataValue;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

public class Probability {

    private List<String> attributes;

    private List<DataRow> data;

    private static DecimalFormat df = new DecimalFormat("0.000");

    private StringBuilder mainStringBuilder = new StringBuilder();

    private StringBuilder frequencyStringBuilder = new StringBuilder();
    private StringBuilder relativeFrequencyStringBuilder = new StringBuilder();

    private Double deviation = 0.0;
    private Map<Double, Integer> frequencyMap = new TreeMap<>();
    private Map<Double, Double> relativeFrequency = new TreeMap<>();
    private Map<Double, Double> nominalDistribution = new TreeMap<>();


    LineChart<Number,Number> lineChart;

    public Probability() {
        this.attributes = new ArrayList<>();
        this.data = new ArrayList<>();
    }

    public Probability(List<String> attributes, List<DataRow> data) {
        this.attributes = attributes;
        this.data = data;
    }

    public void computeProbability(int column, boolean doDisplay){
        this.mainStringBuilder.append("Minimum: ").append(data.stream().min((u,v) -> u.getDoubleListFromData().get(column).compareTo(v.getDoubleListFromData().get(column))).get().getDoubleListFromData().get(column).toString()).append("\n");
        this.mainStringBuilder.append("Maximum: ").append(data.stream().max((u,v) -> u.getDoubleListFromData().get(column).compareTo(v.getDoubleListFromData().get(column))).get().getDoubleListFromData().get(column).toString()).append("\n");

        this.countFrequency(column);
        this.countMeanVarianceDeviationAndMedianAndNominalDistribution(column);
        this.visualizeProbability(doDisplay);
    }


    public void countFrequency(int column) {

        for(int i = 0; i < this.data.size(); i++){

            Double value = data.get(i).getDoubleListFromData().get(column);

            if(frequencyMap.get(value) == null){
                long count = data.stream().filter(f -> f.getDoubleListFromData().stream().filter(uf -> uf.equals(value)).findAny().isPresent()).count();
                frequencyMap.put(value, (int) count);
                frequencyStringBuilder.append(value.toString() + ":\t" + (int)count + "\n");
            }
        }

        this.frequencyMap = this.sortMap(this.frequencyMap);

        for(Map.Entry<Double,Integer> m : this.frequencyMap.entrySet()){
            Double value = (m.getValue()/(double)this.data.size());
            this.relativeFrequency.put(m.getKey(), value);
            relativeFrequencyStringBuilder.append(m.getKey() + ":\t" + df.format(value) + "\n");
        }
    }

    private Map sortMap(Map<Double, Integer> map){
        List<Map.Entry<Double, Integer>> list = new ArrayList<>(map.entrySet());
        list.sort(Map.Entry.comparingByValue());

        Map<Double, Integer> result = new LinkedHashMap<>();
        for (Map.Entry<Double, Integer> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }

    public Double countMeanIndex(int column){
        Double count = 0.0;

        for(DataRow d : this.data){
            count += d.getDoubleListFromData().get(column);
        }

        return count/this.data.size();
    }

    public Double[] countMeanAndVariance(int column){
        Double mean = this.countMeanIndex(column);

        Double variance = 0.0;

        for(DataRow d : this.data){
            variance += Math.pow(d.getDoubleListFromData().get(column) - mean,2);
        }

        variance = variance/this.data.size();

        this.mainStringBuilder.append("Mean: ").append(df.format(mean));
        this.mainStringBuilder.append("\nVariance: ").append(df.format(variance));


        return new Double[]{mean, variance};
    }


    public void countMeanVarianceDeviationAndMedianAndNominalDistribution(int column){
        Double[] meanAndVariance = this.countMeanAndVariance(column);

        this.deviation = Math.sqrt(meanAndVariance[1]);

        this.mainStringBuilder.append("\nDeviation: " + df.format(this.deviation) + "\n");

        double middleIndex = Math.round((this.frequencyMap.size())/2);


        this.mainStringBuilder.append("Median: " + this.frequencyMap.keySet().toArray()[(int)middleIndex] + "\n");

        this.mainStringBuilder.append("Other data: ");
        this.mainStringBuilder.append(df.format((meanAndVariance[0] - this.deviation)) + " " + df.format((meanAndVariance[0] + this.deviation)) + "\n");


        for(Map.Entry<Double,Integer> m : this.frequencyMap.entrySet()){
            this.nominalDistribution.put(m.getKey(),this.countNominalDistribution(m.getKey(),meanAndVariance[0],this.deviation));
        }

//        System.out.println(this.sepalLengthNominalDistribution);

    }

    /**
     * Nominal distribution
     */
    public Double countNominalDistribution(double x, double mean, double deviation){
        return (1/(Math.sqrt(2*Math.PI*Math.pow(deviation,2))))*Math.exp(-((Math.pow((x - mean),2))/(2*Math.pow(deviation,2))));
    }


    public void visualizeProbability(boolean doDisplay) {
        // Add frequencies to the print
        df.applyPattern("0.00000");
        this.mainStringBuilder.append("\nFrequency:\n").append(frequencyStringBuilder.toString());
//        this.mainStringBuilder.append("\nRelative frequency\n").append(relativeFrequencyStringBuilder.toString()).append("\n");

        /**
         * Probability graph
         */
        Stage stage = new Stage();
        stage.setTitle("Probability grap");
        NumberAxis xAxis = new NumberAxis();
        NumberAxis yAxis = new NumberAxis();
        this.lineChart =
                new LineChart<Number,Number>(xAxis,yAxis);
        lineChart.setTitle("Probability");

        xAxis.setAnimated(true);
        yAxis.setAnimated(true);

        xAxis.setLabel("elements");
        yAxis.setLabel("distances");

        XYChart.Series sepalLengthEmpiricSeries = new XYChart.Series();
        sepalLengthEmpiricSeries.setName("Empiric distribution");

        for(Map.Entry<Double,Double> sepal : this.relativeFrequency.entrySet()){

            sepalLengthEmpiricSeries.getData().add(new XYChart.Data(sepal.getKey(), sepal.getValue()));
        }

        XYChart.Series sepalLengthNominalSeries = new XYChart.Series();
        sepalLengthNominalSeries.setName("Nominal distribution");

        for(Map.Entry<Double,Double> sepal : this.nominalDistribution.entrySet()){

            sepalLengthNominalSeries.getData().add(new XYChart.Data(sepal.getKey(), sepal.getValue()));
        }

        lineChart.getData().addAll(sepalLengthEmpiricSeries, sepalLengthNominalSeries);
        Scene scene = new Scene(lineChart, 1000, 1000);

        if(doDisplay) {
            stage.setScene(scene);
            stage.show();

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText("Probability results");
            alert.setContentText(this.mainStringBuilder.toString());
            alert.show();

            System.out.println(this.mainStringBuilder.toString());
        }

    }


    public LineChart<Number, Number> getLineChart() {
        return lineChart;
    }

    public StringBuilder getStringBuilder() {
        return mainStringBuilder;
    }

}
