package algorithms;

import utils.DataRow;

import java.util.*;

public class K_Means {

    private List<String> attributes;


    private List<DataRow> dataRows;

    private List<DataRow> chosenElements;

    private Map<DataRow, List<DataRow>> clusters;

    private Map<DataRow, List<Double>> clustersMean;

    private int K;

    private Random random = new Random();

    public K_Means(int K, List<DataRow> data) {
        this.dataRows = data;

        this.chosenElements = new ArrayList<>();

        this.K = K;

        this.clusters = new HashMap<>();
        this.clustersMean = new HashMap<>();
    }


    public void countKMeans(boolean doDisplay){
        /**
         * Choose K random elements from data
         */
        DataRow chosen;

        for(int i = 0; i < this.K; i++) {

            chosen = this.dataRows.get(this.random.nextInt(this.dataRows.size()));

            while(chosenElements.contains(chosen)){
                chosen = this.dataRows.get(this.random.nextInt(this.dataRows.size()));
            }

            this.chosenElements.add(chosen);
        }

        if(doDisplay) {
            System.out.println("Chosen elements: \n" + chosenElements.toString() + "\n");
        }

        this.countDistancesToTheChosenElements();

//        this.displayCluster();

        // count mean
        this.countClustersMean();

        // count distances to mean
        this.countDistancesToTheMean();


        /**
         * Run algorithm
         */
        List<Integer> clustersSizes;

        int iterations = 0;

        do {
            clustersSizes = this.countClusterSizes();

            this.countClustersMean();

            this.countDistancesToTheMean();

            if(doDisplay) {
                System.out.println(clustersSizes.toString() + "\n" + this.countClusterSizes().toString() + "\n");
            }

//            this.displayCluster();
            iterations++;
        } while (!this.compareClusterSizes(clustersSizes, this.countClusterSizes()));

        if(doDisplay) {
            this.displayCluster();
        }

//        this.countSSE();

        System.out.println("K means iterations: " + iterations);
    }


    public Double countSSE() {
        Double SSE = 0.0;
        Double suma = 0.0;
        Double var = 0.0;


        for (Map.Entry<DataRow, List<DataRow>> clusterMap : this.clusters.entrySet()) {
            suma = 0.0;
            for(DataRow cluster : clusterMap.getValue()){
                var = 0.0;

                for(int i = 0; i < cluster.getDoubleListFromData().size();i++){
                    var += Math.pow((cluster.getDoubleListFromData().get(i) - this.clustersMean.get(clusterMap.getKey()).get(i)),2);
                }

                suma += Math.pow(Math.sqrt(var),2);
//                suma += var;
            }
            SSE += suma;
        }

        return SSE;
    }



    public List<Integer> countClusterSizes(){

        List<Integer> clustersSizes = new ArrayList<>();

        for(Map.Entry<DataRow, List<DataRow>> map : this.clusters.entrySet()){
            clustersSizes.add(map.getValue().size());
        }
        return clustersSizes;
    }

    public boolean compareClusterSizes(List<Integer> list1, List<Integer> list2){
        for(Integer i : list1){
            if(!list2.contains(i)){
                return false;
            }
        }
        return true;
    }



    public void countDistancesToTheChosenElements(){
        List<Double> distances;
        int minIndex = 0;
        double minElem = 0.0;

        for(DataRow row : this.dataRows){

            distances = new ArrayList<>();

            for(DataRow chosenElement : this.chosenElements){

                distances.add(Euclidian_Cosine_Mean.countEuclideanDistanceOfTwoElements(row.getDoubleListFromData(),chosenElement.getDoubleListFromData()));

                minElem = distances.stream().min(Comparator.comparing(Double::doubleValue)).get();

                minIndex = distances.indexOf(minElem);
            }

            List<DataRow> chosenValue = this.clusters.get(this.chosenElements.get(minIndex));
            if(chosenValue == null){
                chosenValue = new ArrayList<>();
                chosenValue.add(row);
            } else {
                chosenValue.add(row);
            }

            this.clusters.put(this.chosenElements.get(minIndex), chosenValue);
        }
    }



    public void countDistancesToTheMean(){
        List<Double> distances;
        int minIndex = 0;
        double minElem = 0.0;

        this.clusters = new HashMap<>();

        for(DataRow row : this.dataRows){

            distances = new ArrayList<>();

            for(Map.Entry<DataRow, List<Double>> map : this.clustersMean.entrySet()){
                distances.add(Euclidian_Cosine_Mean.countEuclideanDistanceOfTwoElements(row.getDoubleListFromData(),map.getValue()));

                minElem = distances.stream().min(Comparator.comparing(Double::doubleValue)).get();

                minIndex = distances.indexOf(minElem);
            }

            List<DataRow> chosenValue = this.clusters.get(this.chosenElements.get(minIndex));
            if(chosenValue == null){
                chosenValue = new ArrayList<>();
                chosenValue.add(row);
            } else {
                chosenValue.add(row);
            }

            this.clusters.put(this.chosenElements.get(minIndex), chosenValue);
        }
    }



    public void displayCluster(){
        for(Map.Entry<DataRow, List<DataRow>> map : this.clusters.entrySet()){
            System.out.println();
            System.out.println(map.getKey().toString() + "\t Size: " + map.getValue().size());
            for(DataRow val : map.getValue()){
                System.out.println("\t" + val.toString());
            }
        }
    }



    public void countClustersMean(){
        for(Map.Entry<DataRow, List<DataRow>> map : this.clusters.entrySet()){
            this.clustersMean.put(map.getKey(), this.countMeanOfGivenData(map.getValue()));
        }
//        System.out.println(this.clustersMean);
    }


    public List<Double> countMeanOfGivenData(List<DataRow> meanData){
        List<Double> result = new ArrayList<>();

        for(int i = 0; i < meanData.get(0).getDoubleListFromData().size(); i++){
            Double count = 0.0;
            for(int j = 0; j < meanData.size(); j++){
                count += meanData.get(j).getDoubleListFromData().get(i);
            }
            result.add(count/meanData.size());
        }

        return result;
    }

    public int getK() {
        return K;
    }

    public void setK(int k) {
        K = k;
    }

    public List<String> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<String> attributes) {
        this.attributes = attributes;
    }

    public List<DataRow> getDataRows() {
        return dataRows;
    }

    public void setDataRows(List<DataRow> dataRows) {
        this.dataRows = dataRows;
    }

    public List<DataRow> getChosenElements() {
        return chosenElements;
    }

    public void setChosenElements(List<DataRow> chosenElements) {
        this.chosenElements = chosenElements;
    }

    public Map<DataRow, List<DataRow>> getClusters() {
        return clusters;
    }

    public void setClusters(Map<DataRow, List<DataRow>> clusters) {
        this.clusters = clusters;
    }

    public Map<DataRow, List<Double>> getClustersMean() {
        return clustersMean;
    }

    public void setClustersMean(Map<DataRow, List<Double>> clustersMean) {
        this.clustersMean = clustersMean;
    }
}

